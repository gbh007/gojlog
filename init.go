package gojlog

import "time"

// переменная с временем начала работы программы
var startTime time.Time

// стандартный логер
var baseLog *Log

// префикс пути который будет обрезан
var trimmedPath []string

// webBufferSize размер буффера вэб сообщений
const webBufferSize = 1000

// init инициализирует первичное состояние пакета
func init() {
	startTime = time.Now()
	baseLog = &Log{
		config:    LogConfig{ErrorInform: true},
		webBuffer: make(chan *logMessage, webBufferSize),
		finish:    make(chan struct{}),
	}
	baseLog.config.Std.Enable = true
	baseLog.config.Std.Raw = true
	baseLog.Start()
}
