package gojlog

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"time"
)

// sendJSON отправляет данные на указаный веб адресс в JSON формате
func (l *Log) sendJSON(m *logMessage) error {
	data, err := json.Marshal(m)
	if err != nil {
		l.innerError(err)
		return err
	}
	req, err := http.NewRequest(http.MethodPost, l.config.Web.URL, bytes.NewBuffer(data))
	if err != nil {
		l.innerError(err)
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	res, err := (&http.Client{}).Do(req)
	if err != nil {
		l.innerError(err)
		return err
	}
	res.Body.Close()
	if res.StatusCode < 200 || res.StatusCode > 299 {
		err = fmt.Errorf("http code %d", res.StatusCode)
		l.innerError(err)
		return err
	}
	return nil
}

// sendForm отправляет данные на указаный веб адресс в PostForm формате
func (l *Log) sendForm(m *logMessage) error {
	res, err := http.PostForm(l.config.Web.URL, url.Values{
		"time":    []string{m.Time.Format(time.RFC3339)},
		"work":    []string{fmt.Sprintln(m.Work)},
		"from":    []string{m.From},
		"level":   []string{m.Level},
		"message": []string{m.Message},
	})
	if err != nil {
		l.innerError(err)
		return err
	}
	res.Body.Close()
	if res.StatusCode < 200 || res.StatusCode > 299 {
		err = fmt.Errorf("http code %d", res.StatusCode)
		l.innerError(err)
		return err
	}
	return nil
}

// writeFile записывает данные в файл с режимом добавления в конец если файл существует
func (l *Log) writeFile(data []byte) {
	if l.config.File.Reopen {
		file, err := os.OpenFile(l.config.File.Filename, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0777)
		l.innerError(err)
		if err != nil {
			return
		}
		defer file.Close()
		_, err = file.Write(data)
		l.innerError(err)
		if err != nil {
			return
		}
		file.Write([]byte("\n"))
	} else {
		if l.file == nil {
			file, err := os.OpenFile(l.config.File.Filename, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0777)
			l.innerError(err)
			if err != nil {
				return
			}
			l.file = file
		}
		_, err := l.file.Write(data)
		l.innerError(err)
		if err != nil {
			l.file = nil
			return
		}
		l.file.Write([]byte("\n"))
	}
}

func (l *Log) runWS() {
	for {
		select {
		case <-l.finish:
			return
		case m := <-l.webBuffer:
			if !l.config.Web.Enable {
				continue
			}
			var err error
			if l.config.Web.Form {
				err = l.sendForm(m)
			} else {
				err = l.sendJSON(m)
			}
			if err != nil {
				if len(l.webBuffer) < webBufferSize {
					l.webBuffer <- m
				}
			}
		}
	}
}
