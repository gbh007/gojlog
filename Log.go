package gojlog

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"runtime"
	"strings"
	"time"
)

type logMessage struct {
	Time    time.Time `json:"time"`    // время сообщения
	Work    int64     `json:"work"`    // время работы программы в наносекундах
	From    string    `json:"from"`    // место возникновения сообщения
	Level   string    `json:"level"`   // уровень сообщения
	Message string    `json:"message"` // сообщение
}

func newLogMessage(level, message string) *logMessage {
	t := time.Now()
	from := "???"
	_, file, line, ok := runtime.Caller(2)
	if len(trimmedPath) > 0 {
		for _, tp := range trimmedPath {
			if strings.Index(file, tp) == 0 {
				file = strings.TrimPrefix(file, tp)
				break
			}
		}
	}
	if ok {
		from = fmt.Sprintf("%s:%d", file, line)
	}
	return &logMessage{
		Time:    t,
		Work:    t.Sub(startTime).Nanoseconds(),
		From:    from,
		Level:   level,
		Message: message,
	}
}

func (lm logMessage) String() string {
	return fmt.Sprintf("[%d] %s %s [%s] %s", lm.Work, lm.Time.Format(time.RFC3339), lm.From, lm.Level, lm.Message)
}

// LogConfig структура для конфигурации логгера
type LogConfig struct {
	ErrorInform bool `json:"error_inform,omitempty"` // логировать внутрение ошибки логгера в поток StdErr
	Std         struct {
		Enable bool `json:"enable"`        // логировать в поток StdErr
		Raw    bool `json:"raw,omitempty"` // использовать сырой формат для StdErr
	} `json:"std,omitempty"`
	File struct {
		Enable   bool   `json:"enable"`           // логировать в файл
		Raw      bool   `json:"raw,omitempty"`    // использовать сырой формат для файла
		Filename string `json:"filename"`         // имя файла (полный путь)
		Reopen   bool   `json:"reopen,omitempty"` // открывать файл для каждой записи заного
	} `json:"file,omitempty"`
	Web struct {
		Enable bool   `json:"enable"` // логировать в Web
		URL    string `json:"url"`    // URL для логирования
		Form   bool   `json:"form"`   // отпровлять данные в виде пост формы
	} `json:"web,omitempty"`
	Debug bool `json:"debug,omitempty"` // выводить отладочные сообщения (тип debug)
}

// Log логгер с урпавлением вывода логов по уровню
type Log struct {
	config    LogConfig
	file      io.Writer
	webBuffer chan *logMessage
	finish    chan struct{}
}

// SetConfig устанавливает новые параметры конфигурации
func (l *Log) SetConfig(config LogConfig) {
	l.config = config
	l.file = nil
}

// NewLog функция для создания нового лога по параметрам текущего за исключением префиксной строки
func NewLog(config LogConfig) *Log {
	return &Log{
		config:    config,
		webBuffer: make(chan *logMessage, webBufferSize),
		finish:    make(chan struct{}),
	}
}

// innerError функция для обработки внутренних ошибок логера
func (l *Log) innerError(err error) {
	if l.config.ErrorInform && err != nil {
		log.Println(err)
	}
}

// Start запускает дочернии процессы необходимые для работы
// отправщик вэб сообщений
func (l *Log) Start() {
	go l.runWS()
}

// Stop останавливает дочерние процессы
func (l *Log) Stop() {
	close(l.finish)
}

// toLog направляет сообщение об ошибки в потоки
func (l *Log) toLog(mess *logMessage) {
	// отлавливаем панику
	// логгер не должен ложить приложение из-за паники
	defer func() {
		p := recover()
		if p != nil {
			l.innerError(fmt.Errorf("panic %+v", p))
		}
	}()
	data, err := json.Marshal(mess)
	if err != nil {
		l.innerError(err)
		return
	}
	if l.config.Std.Enable {
		if l.config.Std.Raw {
			_, err = fmt.Fprintln(os.Stderr, mess)
		} else {
			_, err = fmt.Fprintln(os.Stderr, string(data))
		}
		l.innerError(err)
	}
	if l.config.File.Enable {
		if l.config.File.Raw {
			l.writeFile([]byte(mess.String()))
		} else {
			l.writeFile(data)
		}
	}
	if l.config.Web.Enable {
		if len(l.webBuffer) < webBufferSize {
			l.webBuffer <- mess
		}
	}
}

// Info вывести данные на уровне информирования
func (l *Log) Info(v ...interface{}) *Log {
	l.toLog(newLogMessage("info", fmt.Sprint(v...)))
	return l
}

// Infof вывести данные на уровне информирования при помощи форматированной строки
func (l *Log) Infof(f string, v ...interface{}) *Log {
	l.toLog(newLogMessage("info", fmt.Sprintf(f, v...)))
	return l
}

// Error вывести данные на уровне ошибки
func (l *Log) Error(v ...interface{}) *Log {
	l.toLog(newLogMessage("error", fmt.Sprint(v...)))
	return l
}

// Errorf вывести данные на уровне ошибки при помощи форматированной строки
func (l *Log) Errorf(f string, v ...interface{}) *Log {
	l.toLog(newLogMessage("error", fmt.Sprintf(f, v...)))
	return l
}

// Critical вывести данные на уровне сбоя программы
func (l *Log) Critical(v ...interface{}) *Log {
	l.toLog(newLogMessage("critical", fmt.Sprint(v...)))
	return l
}

// Criticalf вывести данные на уровне сбоя программы при помощи форматированной строки
func (l *Log) Criticalf(f string, v ...interface{}) *Log {
	l.toLog(newLogMessage("critical", fmt.Sprintf(f, v...)))
	return l
}

// Debug вывести данные на уровне отладки
// требуется включенная опция дебага в конфиге
// выводит данные в стандартный поток ошибок опции LogConfig.Std игнорируются
func (l *Log) Debug(v ...interface{}) *Log {
	if !l.config.Debug {
		return l
	}
	_, err := fmt.Fprintln(os.Stderr, newLogMessage("debug", fmt.Sprint(v...)))
	l.innerError(err)
	return l
}

// Debugf вывести данные на уровне отладки при помощи форматированной строки
// требуется включенная опция дебага в конфиге
// выводит данные в стандартный поток ошибок опции LogConfig.Std игнорируются
func (l *Log) Debugf(f string, v ...interface{}) *Log {
	if !l.config.Debug {
		return l
	}
	_, err := fmt.Fprintln(os.Stderr, newLogMessage("debug", fmt.Sprintf(f, v...)))
	l.innerError(err)
	return l
}

// Warning вывести данные на уровне предупреждения
func (l *Log) Warning(v ...interface{}) *Log {
	l.toLog(newLogMessage("warning", fmt.Sprint(v...)))
	return l
}

// Warningf вывести данные на уровне предупреждения при помощи форматированной строки
func (l *Log) Warningf(f string, v ...interface{}) *Log {
	l.toLog(newLogMessage("warning", fmt.Sprintf(f, v...)))
	return l
}

// IfErr вывести данные на уровне ошибки
// выводит данные только если ошибка не пустая
func (l *Log) IfErr(err error) *Log {
	if err != nil {
		l.toLog(newLogMessage("error", err.Error()))
	}
	return l
}
