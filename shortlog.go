package gojlog

import (
	"bytes"
	"fmt"
	"time"
)

// ShortLog структура для сохранения временых логов
// (короткие логи, подобие отчета со временем действия)
type ShortLog struct {
	bytes.Buffer
	lastReset  time.Time
	ShowDebug  bool // показывать сообщения отладки
	ShowTime   bool // показывать временую метку заместо длительности
	TimeFormat string
}

// Reset сбрасывает записи лога и время
func (l *ShortLog) Reset() {
	l.Buffer.Reset()
	l.lastReset = time.Now()
}

// newLogMessage записывает форматированое сообщение в лог
func (l *ShortLog) newLogMessage(level, message string) {
	t := time.Now()
	// если опция показывать время то указываем его
	if l.ShowTime {
		f := l.TimeFormat
		if f == "" {
			f = time.RFC1123
		}
		l.WriteString(fmt.Sprintf("%s [%s] %s\n", t.Format(f), level, message))
		return
	}
	d := time.Duration(0)
	if !l.lastReset.IsZero() {
		d = t.Sub(l.lastReset)
	}
	d = d / time.Millisecond * time.Millisecond
	f := fmt.Sprintf("%v [%s] %s\n", d, level, message)
	l.WriteString(f)
}

// Info сохранить данные в лог на уровне информирования
func (l *ShortLog) Info(v ...interface{}) *ShortLog {
	l.newLogMessage("info", fmt.Sprint(v...))
	return l
}

// Infof сохранить данные в лог на уровне информирования при помощи форматированной строки
func (l *ShortLog) Infof(f string, v ...interface{}) *ShortLog {
	l.newLogMessage("info", fmt.Sprintf(f, v...))
	return l
}

// Error сохранить данные в лог на уровне ошибки
func (l *ShortLog) Error(v ...interface{}) *ShortLog {
	l.newLogMessage("error", fmt.Sprint(v...))
	return l
}

// Errorf сохранить данные в лог на уровне ошибки при помощи форматированной строки
func (l *ShortLog) Errorf(f string, v ...interface{}) *ShortLog {
	l.newLogMessage("error", fmt.Sprintf(f, v...))
	return l
}

// Critical сохранить данные в лог на уровне сбоя программы
func (l *ShortLog) Critical(v ...interface{}) *ShortLog {
	l.newLogMessage("critical", fmt.Sprint(v...))
	return l
}

// Criticalf сохранить данные в лог на уровне сбоя программы при помощи форматированной строки
func (l *ShortLog) Criticalf(f string, v ...interface{}) *ShortLog {
	l.newLogMessage("critical", fmt.Sprintf(f, v...))
	return l
}

// Debug сохранить данные на уровне отладки
// требуется включенная опция дебага в конфиге
func (l *ShortLog) Debug(v ...interface{}) *ShortLog {
	if !l.ShowDebug {
		return l
	}
	l.newLogMessage("debug", fmt.Sprint(v...))
	return l
}

// Debugf сохранить данные на уровне отладки при помощи форматированной строки
// требуется включенная опция дебага в конфиге
func (l *ShortLog) Debugf(f string, v ...interface{}) *ShortLog {
	if !l.ShowDebug {
		return l
	}
	l.newLogMessage("debug", fmt.Sprintf(f, v...))
	return l
}

// Warning сохранить данные в лог на уровне предупреждения
func (l *ShortLog) Warning(v ...interface{}) *ShortLog {
	l.newLogMessage("warning", fmt.Sprint(v...))
	return l
}

// Warningf сохранить данные в лог на уровне предупреждения при помощи форматированной строки
func (l *ShortLog) Warningf(f string, v ...interface{}) *ShortLog {
	l.newLogMessage("warning", fmt.Sprintf(f, v...))
	return l
}

// IfErr сохранить данные в лог на уровне ошибки
// сохраняет данные только если ошибка не пустая
func (l *ShortLog) IfErr(err error) *ShortLog {
	if err != nil {
		l.newLogMessage("error", err.Error())
	}
	return l
}
