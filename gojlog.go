package gojlog

import (
	"fmt"
	"os"
	"path"
	"runtime"
)

// GetBaseLog возвращает стандартный логер
func GetBaseLog() *Log { return baseLog }

// SetConfig устанавливает новые параметры конфигурации общего логера
func SetConfig(config LogConfig) {
	baseLog.config = config
	baseLog.file = nil
}

// SetTrimPath устанавливает значение обрезания пути
// влияет на поле LogMessage.From
func SetTrimPath(trim string) {
	trimmedPath = []string{trim}
}

// AddTrimPath добавляет значение обрезания пути
// влияет на поле LogMessage.From
func AddTrimPath(trim string) {
	trimmedPath = append(trimmedPath, trim)
}

// SetTrimPathAuto автоматически устанавливает значение обрезания пути
// влияет на поле LogMessage.From
// путь обрезки устанавливается до названия текущего пакета
// если данные собираются в корне файловой системы то путь обрезается до лидирующего слеша\
// примеры
// C:/main.go -> /main.go
// /main.go -> /main.go
// /test/main.go -> test/main.go
// /project/test/main.go -> test/main.go
func SetTrimPathAuto() {
	_, file, _, ok := runtime.Caller(1)
	if ok {
		p, _ := path.Split(file)
		p = path.Dir(p)
		r, n := path.Split(p)
		switch {
		// корень файловой системы windows
		case len(r) == 0:
			trimmedPath = []string{p}
		// корень файловой системы linux
		case len(n) == 0:
			trimmedPath = []string{}
		default:
			trimmedPath = []string{r}
		}
	}
}

// AddTrimPathAuto автоматически добавляет значение обрезания пути
// влияет на поле LogMessage.From
// путь обрезки устанавливается до названия текущего пакета
// если данные собираются в корне файловой системы то путь обрезается до лидирующего слеша\
// примеры
// C:/main.go -> /main.go
// /main.go -> /main.go
// /test/main.go -> test/main.go
// /project/test/main.go -> test/main.go
func AddTrimPathAuto() {
	_, file, _, ok := runtime.Caller(1)
	if ok {
		p, _ := path.Split(file)
		p = path.Dir(p)
		r, n := path.Split(p)
		switch {
		// корень файловой системы windows
		case len(r) == 0:
			trimmedPath = append(trimmedPath, p)
		// корень файловой системы linux
		case len(n) == 0:
		default:
			trimmedPath = append(trimmedPath, r)
		}
	}
}

// Info вывести данные на уровне информирования в общий логгер
func Info(v ...interface{}) *Log {
	baseLog.toLog(newLogMessage("info", fmt.Sprint(v...)))
	return baseLog
}

// Infof вывести данные на уровне информирования при помощи форматированной строки в общий логгер
func Infof(f string, v ...interface{}) *Log {
	baseLog.toLog(newLogMessage("info", fmt.Sprintf(f, v...)))
	return baseLog
}

// Error вывести данные на уровне ошибки в общий логгер
func Error(v ...interface{}) *Log {
	baseLog.toLog(newLogMessage("error", fmt.Sprint(v...)))
	return baseLog
}

// Errorf вывести данные на уровне ошибки при помощи форматированной строки в общий логгер
func Errorf(f string, v ...interface{}) *Log {
	baseLog.toLog(newLogMessage("error", fmt.Sprintf(f, v...)))
	return baseLog
}

// Critical вывести данные на уровне сбоя программы в общий логгер
func Critical(v ...interface{}) *Log {
	baseLog.toLog(newLogMessage("critical", fmt.Sprint(v...)))
	return baseLog
}

// Criticalf вывести данные на уровне сбоя программы при помощи форматированной строки в общий логгер
func Criticalf(f string, v ...interface{}) *Log {
	baseLog.toLog(newLogMessage("critical", fmt.Sprintf(f, v...)))
	return baseLog
}

// Debug вывести данные на уровне отладки
// требуется включенная опция дебага в конфиге
// выводит данные в стандартный поток ошибок опции LogConfig.Std игнорируются
func Debug(v ...interface{}) *Log {
	if !baseLog.config.Debug {
		return baseLog
	}
	_, err := fmt.Fprintln(os.Stderr, newLogMessage("debug", fmt.Sprint(v...)))
	baseLog.innerError(err)
	return baseLog
}

// Debugf вывести данные на уровне отладки при помощи форматированной строки
// требуется включенная опция дебага в конфиге
// выводит данные в стандартный поток ошибок опции LogConfig.Std игнорируются
func Debugf(f string, v ...interface{}) *Log {
	if !baseLog.config.Debug {
		return baseLog
	}
	_, err := fmt.Fprintln(os.Stderr, newLogMessage("debug", fmt.Sprintf(f, v...)))
	baseLog.innerError(err)
	return baseLog
}

// Warning вывести данные на уровне предупреждения в общий логгер
func Warning(v ...interface{}) *Log {
	baseLog.toLog(newLogMessage("warning", fmt.Sprint(v...)))
	return baseLog
}

// Warningf вывести данные на уровне предупреждения при помощи форматированной строки в общий логгер
func Warningf(f string, v ...interface{}) *Log {
	baseLog.toLog(newLogMessage("warning", fmt.Sprintf(f, v...)))
	return baseLog
}

// IfErr вывести данные на уровне ошибки в общий логгер
// выводит данные только если ошибка не пустая
func IfErr(err error) *Log {
	if err != nil {
		baseLog.toLog(newLogMessage("error", err.Error()))
	}
	return baseLog
}
